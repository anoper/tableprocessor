class NumbersGenerator
  def self.primes(numbers)
    return [] if numbers == 0
    primes = []
    num = 2

    loop do
      primes << num if is_prime?(num)
      num += 1
      break if primes.count == numbers
    end

    primes
  end

  def self.is_prime?(number)
    return false if number < 2
    upto=Math.sqrt(number)

    (2..upto).none? do |k|
      0 == number % k
    end
  end
end