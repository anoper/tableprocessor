class CharsProcessor
  def initialize chars, operation
    @chars = chars
    @operation = operation
  end

  def elements
    @chars
  end

  def to_table_lines
    lines = []

    elements.each do |num|
      line = []
      elements.each do |another_num|
        line << execute(num, another_num, @operation)
      end
      lines << line
    end

    lines
  end

  def addition x, y
    x + y
  end

  private
  def execute(x, y, operation)
    if self.respond_to?(operation)
      send(operation, x, y)
    else
      raise ArgumentError, "Operation #{operation} is not supported. Try e.g. 'multiply'"
    end
  end

end