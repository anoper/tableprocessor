#!/usr/bin/env ruby

require 'optparse'
require_relative 'table'

class TableProcessor
  def initialize
    options = parse_options
    table = Table.new(options)
    table.render
  end

  private
  def parse_options
    options = {}
    OptionParser.new do |opts|
      opts.on('--primes Int') do |value|
        options[:primes] = value.to_i
      end

      opts.on('--chars Int') do |value|
        options[:chars] = value.to_i
      end
    end.parse!

    options
  end
end

TableProcessor.new