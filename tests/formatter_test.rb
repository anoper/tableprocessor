require 'test/unit'
require_relative '../table.rb'

class TestTable < Test::Unit::TestCase

  def test_returns_string_representation_of_table
    table = Table.new({primes: 10})
    assert(table.to_s == table_primes_string)
  end

  def test_returns_string_for_the_chars_table
    table = Table.new({chars: 10})
    assert(table.to_s == table_chars_string)
  end

  def table_chars_string
  "   |  a  b  c  d  e  f  g  h  i  j\n"\
  "---+------------------------------\n"\
  " a | aa ab ac ad ae af ag ah ai aj\n"\
  " b | ba bb bc bd be bf bg bh bi bj\n"\
  " c | ca cb cc cd ce cf cg ch ci cj\n"\
  " d | da db dc dd de df dg dh di dj\n"\
  " e | ea eb ec ed ee ef eg eh ei ej\n"\
  " f | fa fb fc fd fe ff fg fh fi fj\n"\
  " g | ga gb gc gd ge gf gg gh gi gj\n"\
  " h | ha hb hc hd he hf hg hh hi hj\n"\
  " i | ia ib ic id ie if ig ih ii ij\n"\
  " j | ja jb jc jd je jf jg jh ji jj\n"\
  end

  def table_primes_string
     "    |   2   3   5   7  11  13  17  19  23  29\n" \
     "----+----------------------------------------\n" \
     " 2  |   4   6  10  14  22  26  34  38  46  58\n" \
     " 3  |   6   9  15  21  33  39  51  57  69  87\n" \
     " 5  |  10  15  25  35  55  65  85  95 115 145\n" \
     " 7  |  14  21  35  49  77  91 119 133 161 203\n" \
     " 11 |  22  33  55  77 121 143 187 209 253 319\n" \
     " 13 |  26  39  65  91 143 169 221 247 299 377\n" \
     " 17 |  34  51  85 119 187 221 289 323 391 493\n" \
     " 19 |  38  57  95 133 209 247 323 361 437 551\n" \
     " 23 |  46  69 115 161 253 299 391 437 529 667\n" \
     " 29 |  58  87 145 203 319 377 493 551 667 841\n" \
  end
end