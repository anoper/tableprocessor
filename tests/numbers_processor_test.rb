require 'test/unit'
require_relative '../numbers_processor'

class TestNumbersProcessor < NumbersProcessor
  def initialize; end
end

class NumbersProcessorTest < Test::Unit::TestCase
  def test_multiply_numbers
    numbers_processor = TestNumbersProcessor.new
    assert(numbers_processor.multiply(2, 3)) == 6
  end

  def test_returns_table_lines
    numbers_processor = NumbersProcessor.new([2,3], :multiply)
    assert(numbers_processor.to_table_lines == [[4, 6], [6, 9]])
  end

  def test_table_lines_returns_empty_array_for_no_numbers
    numbers_processor = NumbersProcessor.new([], :multiply)
    assert(numbers_processor.to_table_lines == [])
  end

  def test_raise_exception_when_invalid_number
    numbers_processor = NumbersProcessor.new([1, 'b'], :multiply)
    assert_raise TypeError do
      numbers_processor.to_table_lines
    end
  end

  def test_raise_exception_for_invalid_operation
    numbers_processor = NumbersProcessor.new([2,3], :sum)

    assert_raise ArgumentError do
      numbers_processor.to_table_lines
    end
  end
end