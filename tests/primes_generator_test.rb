require 'test/unit'
require_relative '../numbers_generator'

class PrimesGeneratorTest < Test::Unit::TestCase
  def test_generates_primes
    first_100_primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
     73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173,
     179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277,
     281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397,
     401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509,
     521, 523, 541]

    assert(NumbersGenerator.primes(100) == first_100_primes)
  end

  def test_generates_no_primes
    assert(NumbersGenerator.primes(0) == [])
  end

  def test_generates_1_prime
    assert(NumbersGenerator.primes(1) == [2])
  end

  def test_returns_true_for_prime_number
    assert(NumbersGenerator.is_prime?(3) == true)
  end

  def test_returns_true_for_2
    assert(NumbersGenerator.is_prime?(2) == true)
  end

  def test_returns_false_for_1
    assert(NumbersGenerator.is_prime?(1) == false)
  end

  def test_returns_false_for_negative_number
    assert(NumbersGenerator.is_prime?(-1) == false)
  end
end