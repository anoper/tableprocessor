require 'test/unit'
require_relative '../processor'


class TestProcessor < Processor
  def initialize; end
end

class ProcessorTest < Test::Unit::TestCase
  def test_build_generator_returns_chars
    processor = TestProcessor.new()
    assert(processor.build_generator({:chars => 10}).class == CharsProcessor)
  end
end