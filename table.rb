require_relative 'processor'
require_relative 'formatter'

class Table
  def initialize(args)
    @processor = Processor.new(args)
    table = self.process
    @formatter = Formatter.new(table)
  end

  def process
    @processor.process_table
  end

  def render
    @formatter.render
  end

  def to_s
    @formatter.to_table
  end
end