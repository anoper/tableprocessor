class Formatter
  def initialize table
    @table = table
  end

  def to_table
    header + separate_line + body
  end

  def render
    printf to_table
  end

  private
  def header
    empty_element + '|' + parse_row(header_row) + break_row
  end

  def body
    table_body = ""
    body_rows.each_with_index do |row, index|
      # leading prime number with divider
      table_body << centred_element(header_row[index].to_s, '|') 
      table_body << parse_row(row)
      table_body << break_row
    end

    table_body
  end

  def parse_row row_a, leading_divider=nil
    row = ""
    row_a.each_with_index do |element, index|

      if index == 0 && !leading_divider.nil?
        row += centred_element(element.to_s, '|')
      else
        row += spaced_element(element.to_s)
      end

    end
    row
  end

  def separate_line
    ('-'*column_width) + '+' + ('-'*table_width) + break_row
  end

  def table_width
    header_row.count*(column_width)
  end

  def spaced_element(element)
    element.rjust(column_width, space)
  end

  def centred_element(element, divider='')
    element.center(column_width, space) + divider
  end

  def empty_element
    spaced_element(space)
  end

  def table_size
    @table.length
  end

  def column_width
    (size_of_longest_element+1) 
  end

  def size_of_longest_element
    @table.flatten.max.to_s.size
  end
  def header_row
    @table[0]
  end

  def body_rows
    @table[1]
  end

  def break_row
    "\n"
  end

  def space
    ' '
  end
end