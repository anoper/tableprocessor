class NumbersProcessor
  def initialize numbers, operation
    @numbers = numbers
    @operation = operation
  end

  def elements
    @numbers
  end

  def to_table_lines
    lines = []

    @numbers.each do |num|
      line = []
      @numbers.each do |another_num|
        line << execute(num, another_num, @operation)
      end
      lines << line
    end

    lines
  end

  def multiply x, y
    x * y
  end

  private
  def execute(x, y, operation)
    if self.respond_to?(operation)
      send(operation, x, y)
    else
      raise ArgumentError, "Operation #{operation} is not supported. Try e.g. 'multiply'"
    end
  end
end