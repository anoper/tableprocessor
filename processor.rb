require_relative 'numbers_processor'
require_relative 'numbers_generator'
require_relative 'chars_processor'

class Processor
  def initialize(args)
    @generator = build_generator(args)
    @header = @generator.elements
    @lines = []
  end

  def process_table
    @lines << @header
    @lines << @generator.to_table_lines
    @lines
  end

  def build_generator args
    type = args.keys.first
    
    case type
    when :primes
      numbers_quantity = args[type]
      numbers = NumbersGenerator.primes(numbers_quantity)
      NumbersProcessor.new(numbers, :multiply)
    when :chars
      chars_quantity = args[type]
      chars = ['a']
      (chars_quantity - 1).times{|i| chars << chars.last.succ }
      CharsProcessor.new(chars, :addition)
    else
      raise ArgumentError, "Type #{type} is not supported"
    end
  end
end