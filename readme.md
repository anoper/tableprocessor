#TableProcessor
Generates number of elements of specific type and format it into the table.

##Instructions
Run application with:
  
    ./table_processor.rb --primes 10

Run tests with:

    rake test